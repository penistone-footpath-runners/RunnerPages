#!/usr/bin/env python3
import os

PATHTOHTML = "Runner Pages/"

COMPDIRECTORIES = {
    "Club Standards": [
        "ClubStandards",
        '../ClubStandards/index.html">Club Standards</a></h3><em>No club standards.',
    ],
    "Club Records": [
        "Records/AllAllAllAll.html",
        '../Records/AllAllAllAll.html">Club Records</a></h3><em>No club records.',
    ],
    "Club Championship": [
        "Championship2019",
        '../Championship2019/Challenge.html">Club Championship</a></h3><em>No Club Championship races.',
    ],
    "Fell League": [
        "FellLeague2020",
        '../FellLeague2019/Races.html">Fell League</a></h3><em>No qualifying fell races.',
    ],
    "parkrun Competition": [
        "parkrun2020",
        '../parkrun2019/Overall.html">parkrun Competition</a></h3><em>No parkruns.',
    ],
}

HEAD = '<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="charset=utf-8"><link rel="icon" href="../icon.png"><title>PFR RUNNER</title><link rel="stylesheet" href="../style.css"></head><body><div class="maindiv"><img class="logo" src="../logo.png" alt="Penistone Footpath Runners & AC"><div class="title"><h1>Penistone Footpath Runners &amp; AC</h1><h2>RUNNER</h2></div>'

if not os.path.exists(PATHTOHTML):
    os.makedirs(PATHTOHTML)

try:
    with open("pages.txt", "r") as f:
        runners = eval(f.read())
except FileNotFoundError:
    with open("pages.txt", "w") as f:
        f.write("{}")
        runners = {}

# Make per-runner pages
for runner in runners:
    page = HEAD.replace("RUNNER", runner)
    for competition in COMPDIRECTORIES:
        comp_url = COMPDIRECTORIES[competition][0]
        # Check if the runner does that competition
        if competition not in runners[runner]:
            page += f'<h3><a class="headinglink" href="{COMPDIRECTORIES[competition][1]}</em><br><br>'
            continue
        # No / implies that the runner has their own page, so link to it
        if "/" in comp_url:
            page += f'<h3><a class="headinglink" href="../{comp_url}">{competition}</a></h3>'
        else:
            page += f'<h3><a class="headinglink" href="../{comp_url}/{runner}.html">{competition}</a></h3>'
        page += runners[runner][competition]
        page += "<br><br>"
    with open(f"{PATHTOHTML}{runner}.html", "w") as f:
        f.write(page)

# Make list of runners
maintable = HEAD.replace("RUNNER", "List of Runners") + "<table>"
for runner in sorted(runners.keys(), key=lambda x: x.split(" ")[1::-1]):
    maintable += f'<tr><td style="text-align:left"><a href="{runner}.html">{runner}</a></td></tr>'
maintable += "</table></div></body></html>"
with open(f"{PATHTOHTML}index.html", "w") as f:
    f.write(maintable)
